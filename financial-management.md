---
layout: page
title: financial-management.title
namespace: financial-management
permalink: /gestao-financeira/
permalink_en: /financial-management/
header-image: true
main-phrase-tkey: financial-management.main-phrase
caption: <a href="http://www.freepik.com/free-photo/finances-saving-economy-concept-female-accountant-or-banker-use-calculator_1211587.htm">Designed by Freepik</a>
---
<div class="tabs-container">
    <ul class="tab-area">
        <li class="tab n1 shadow-box" data-opens="t1">
            <h2>{% t financial-management.management.title %}</h2>
        </li>

        <li class="tab n2  shadow-box" data-opens="t2">
            <h2>{% t financial-management.register.title %}</h2>
        </li>
    </ul>

    <div class="tab-box" id="t1">
        {% t financial-management.management.text %}
    </div>


    <div class="tab-box" id="t2">
        {% t financial-management.register.text %}
    </div>
</div>
