---
layout: page
title: work.title
namespace: work
permalink: /nossaequipe/
permalink_en: /ourteam/
header-image: true
caption: <a href="http://br.freepik.com/fotos-gratis/empresario-aponta-graficos-e-simbolos_985257.htm">Designed by Freepik</a>
---
<div class="tabs-container">
    <ul class="tab-area">
        <li class="tab n1 shadow-box" data-opens="t1" id="{% t work.people-management.id %}">
            <h2>{% t work.people-management.title %}</h2>
        </li>

        <li class="tab n2 shadow-box" data-opens="t2" id="{% t work.work-with-us.id %}">
            <h2>{% t work.work-with-us.title %}</h2>
        </li>
    </ul>

    <div class="tab-box" id="t1">
        {% t work.people-management.text %}
    </div>

    <div class="tab-box" id="t2">
        <div class="background-inner-image" style="background-image: url('{{site.img_path}}/work/work.jpg')">
            <span class="phrase">{% t work.work-with-us.phrase %}</span>
            <span class="caption"><a href="http://br.freepik.com/fotos-gratis/o-trabalho-em-equipe-empresarial-junta-as-maos-juntas-conceito-de-trabalho-em-equipe-empresarial_1185949.htm">Designed by Freepik</a></span>
        </div>
        {% t work.work-with-us.text %}
        
    </div>
</div>





