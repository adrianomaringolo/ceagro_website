---
layout: page
title: contact.title
namespace: contact
permalink: /contato/
permalink_en: /contact/
header-image: true
caption: <a href="http://br.freepik.com/fotos-gratis/user-people-network-circuit-board-conexao-conexao-tecnologia_1198382.htm">Designed by Freepik</a>
---
<section class="contact-area page">
    <div class="container-info">
        <div class="contact-form">
            <form>
            <label>Nome *</label>
            <input type="text" required>
            <label>Email *</label>
            <input type="email" required>
            <label>Empresa</label>
            <input type="text">
            <label>Mensagem *</label>
            <textarea name="" id="" cols="30" rows="7" required></textarea>
            <div style="text-align: right">
                <button type="submit">Enviar</button>
            </div>
            </form>
        </div>
    
        <div class="contact-info">
            <div class="box">
                <h4>Trabalhe conosco!</h4>
                <a href="{% tl work %}" class="shadow-box primary">
                <i class="fa fa-id-card" aria-hidden="true"></i>Envie seu currículo
                </a>
            </div>
            <div class="box">
                <h4>Sede</h4>
                <p>Rua Antonio Lapa, 280 – 8º andar<br>
                    Cambuí, Campinas - São Paulo<br>
                    CEP: 113025-240<br>
                    Telefone: + 55 19 3114-8600</p>
                <hr>
                <a href="https://www.google.com.br/maps/place/Av.+Cel.+Silva+Telles,+977+-+Cambu%C3%AD,+Campinas+-+SP,+13024-001/@-22.8940617,-47.0479989,20z/data=!4m5!3m4!1s0x94c8cf5a84a9530b:0x8dcf84062108f3f5!8m2!3d-22.8942668!4d-47.0477823?hl=pt-BR" 
                class="shadow-box primary">
                <i class="fa fa-map-marker" aria-hidden="true"></i>Abrir no Google Maps</a>
            </div>
            </div>
            
        </div>
</section>