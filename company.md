---
layout: page
title: company.title
namespace: company
permalink: /empresa/
permalink_en: /company/
header-image: true
main-phrase-tkey: company.main-phrase
---

{% t company.text %}

<div class="accordion-section">
	<h2 class="accordion" data-open="1" id="{% t company.mission.id %}">{% t company.mission.title %}</h2>
	<div class="accordion" data-order="1">
		{% t company.mission.text %}
	</div>

    <h2 class="accordion" data-open="2" id="{% t company.history.id %}">{% t company.history.title %}</h2>
	<div class="accordion tabs-container" data-order="2">
        <ul class="tab-area label-tags">
            <li>Anos: </li>
            <li class="tab n1 shadow-box selected" data-opens="h1">1987</li>
            <li class="tab n2 shadow-box" data-opens="h2">1991</li>
            <li class="tab n3 shadow-box" data-opens="h3">2002</li>
            <li class="tab n4 shadow-box" data-opens="h4">2005</li>
            <li class="tab n5 shadow-box" data-opens="h5">2010</li>
            <li class="tab n6 shadow-box" data-opens="h6">2011</li>
            <li class="tab n6 shadow-box" data-opens="h7">2012</li>
            <li class="tab n6 shadow-box" data-opens="h8">2013</li>
        </ul>
        <div class="tab-box boxes" id="h1">{% t company.history.text.year1987 %}</div>
        <div class="tab-box boxes" id="h2">{% t company.history.text.year1991 %}</div>
        <div class="tab-box boxes" id="h3">{% t company.history.text.year2002 %}</div>
        <div class="tab-box boxes" id="h4">{% t company.history.text.year2005 %}</div>
        <div class="tab-box boxes" id="h5">{% t company.history.text.year2010 %}</div>
        <div class="tab-box boxes" id="h6">{% t company.history.text.year2011 %}</div>
        <div class="tab-box boxes" id="h7">{% t company.history.text.year2012 %}</div>
        <div class="tab-box boxes" id="h8">{% t company.history.text.year2013 %}</div>
    </div>

    <h2 class="accordion" data-open="3" id="{% t company.sites.id %}">{% t company.sites.title %}</h2>
	<div class="accordion tabs-container" data-order="3">
        <ul class="tab-area label-tags">
            <li>Estados: </li>
            <li class="tab n1 shadow-box selected" data-opens="t1">BA</li>
            <li class="tab n2 shadow-box" data-opens="t2">GO</li>
            <li class="tab n3 shadow-box" data-opens="t3">MS</li>
            <li class="tab n4 shadow-box" data-opens="t4">MT</li>
            <li class="tab n5 shadow-box" data-opens="t5">PR</li>
            <li class="tab n6 shadow-box" data-opens="t6">SP</li>
        </ul>

        <div class="tab-box boxes" id="t1">{% t company.sites.text.locationsBA %}</div>
        <div class="tab-box boxes" id="t2">{% t company.sites.text.locationsGO %}</div>
        <div class="tab-box boxes" id="t3">{% t company.sites.text.locationsMS %}</div>
        <div class="tab-box boxes" id="t4">{% t company.sites.text.locationsMT %}</div>
        <div class="tab-box boxes" id="t5">{% t company.sites.text.locationsPR %}</div>
        <div class="tab-box boxes" id="t6">{% t company.sites.text.locationsSP %}</div>
    </div>

    <h2 class="accordion" data-open="4">{% t company.agroscience.title %}</h2>
	<div class="accordion" data-order="4">
		{% t company.agroscience.text %}
	</div>

    <h2 class="accordion" data-open="5">{% t company.agroextra.title %}</h2>
	<div class="accordion" data-order="5">
		{% t company.agroextra.text %}
	</div>
</div>


<script>
$(document).ready(function(){
	$('h2.accordion').click(function(){
		$('div.accordion[data-order='+$(this).attr("data-open")+']').slideToggle("slow");
	});

    var hash = window.location.hash;

    if(hash) {
        jQuery("h2.accordion" + hash).click();

        jQuery('html, body').animate({
            scrollTop:jQuery(hash).offset().top
        }, 800, function(){
            window.location.hash = hash;
        });
    }
});
</script>
