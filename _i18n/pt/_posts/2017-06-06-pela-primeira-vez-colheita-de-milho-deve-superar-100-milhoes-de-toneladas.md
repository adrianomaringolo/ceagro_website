---
layout: post
title:  "Pela primeira vez, colheita de milho deve superar 100 milhões de toneladas"
date:   2017-06-06 00:00:00 -0300
lang: pt
main-img: "news-2017-06-06.jpg"
main-img-ref: ''
---

<p class="header">Segundo consultoria Céleres, clima regular e "bom andamento" da safra de soja contribuem para a produção do grão</p>

A colheita de milho no Brasil deve alcançar 100,7 milhões de toneladas na safra 2016/2017. A previsão é da consultoria Céleres, divulgada nesta terça-feira (6). Caso se confirme, essa será a primeira vez que a produção ultrapassará o nível de 100 milhões de toneladas. O recorde é de 84,7 milhões de toneladas, registrado na safra 2014/15.

Segundo a consultoria, se a área da segunda safra de milho se manter em 11,3 milhões ha, crescimento de 8,4% em relação à temporada passada, a produtividade em 2016/17 deverá ser de 5,9 t/ha, o que representa uma recuperação de 37% na comparação com 2015/16.

De acordo com a Célere, “a safra de milho inverno foi beneficiada pelo clima regular e dentro do necessário em praticamente todas as regiões produtoras do País” e “pelo bom andamento da safra de soja, que permitiu que o plantio da segunda safra ocorresse dentro do ideal”.

A projeção da Céleres para exportação do milho em 2017 é de 30 milhões t, aumento de 37% em relação ao ano anterior. Sobre o mercado interno, a análise afirma que “se desenha um cenário de reabastecimento” e “de um balanço produtivo muito mais confortável que ano passado, com estoques finais aumentando para 16,4 milhões t, o dobro do observado na safra passada”.

<hr>
Fonte: [Portal Brasil](http://www.brasil.gov.br/economia-e-emprego/2017/06/pela-primeira-vez-colheita-de-milho-deve-superar-100-milhoes-de-toneladas)