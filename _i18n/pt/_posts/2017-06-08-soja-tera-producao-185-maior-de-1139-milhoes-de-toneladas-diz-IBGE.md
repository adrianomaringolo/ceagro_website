---
layout: post
title:  "Soja terá produção 18,5% maior, de 113,9 milhões de toneladas, diz IBGE"
date:   2017-06-08 00:00:00 -0300
lang: pt
main-img: "news-2017-06-08.jpg"
main-img-ref: ''
---

Rio, 8/6 – O Levantamento Sistemático da Produção Agrícola de maio estima uma safra recorde de 113,9 milhões de toneladas de soja, 18,5% acima da produção verificada em 2016, informou o Instituto Brasileiro de Geografia e Estatística (IBGE). Já a área plantada de soja deverá subir 2,1% em relação a 2016, para 33,9 milhões de hectares. Com isso, o IBGE estima rendimento médio 3.367 kg/ha.

“Na presente safra, o maior produtor é o Mato Grosso, com 30,7 milhões de toneladas, ou 27% do total nacional. O Paraná é o segundo produtor, com 19,5 milhões de toneladas, ou 17,2% do total nacional. O Rio Grande do Sul aparece como terceiro produtor, participando com 16,3% do total nacional”, diz a nota divulgada pelo IBGE.

Segundo o instituto, a estimativa para a produção da soja voltou a subir em maio, por causa do Rio Grande do Sul, “que aguarda uma produção de 18,6 milhões de toneladas, aumento de 7,6% em relação ao mês anterior”.

Houve reavaliação positiva de 0,6% na área plantada e a ser colhida. “O responsável por esse aumento de 1,3 milhão de toneladas a mais em relação ao mês anterior foi o rendimento médio, que aumentou 7,0% em decorrência, principalmente, do clima que beneficiou as lavouras no campo”, diz o IBGE.

<hr>
Fonte: [IstoÉ](http://istoe.com.br/soja-tera-producao-185-maior-de-1139-milhoes-de-toneladas-diz-ibge/)