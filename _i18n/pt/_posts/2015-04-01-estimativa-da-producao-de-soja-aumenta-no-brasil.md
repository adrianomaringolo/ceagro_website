---
layout: post
title:  "Estimativa da produção de soja aumenta no Brasil"
date:   2015-04-01 14:52:03 -0300
lang: pt
main-img: news-2015-04-01.jpg
main-img-ref: ''
---
De acordo com a consultoria Agroconsult, a safra brasileira de soja 2014/2015 deve registrar novo recorde, com 95,8 milhões de toneladas, volume 11% maior frente à safra anterior. O resultado reflete, principalmente, as chuvas abundantes e regulares no Sul do País desde o início do plantio, o que aumentou a produtividade na região para cerca de 50 sacas por hectare e equilibrou parte da perda por estiagem em outras localidades como Goiás, Minas Gerais e parte da Bahia e do Piauí.

Os Estados do Rio Grande do Sul (RS) e do Mato Grosso (MT) são destaques nessa safra. O RS deve produzir 15 milhões de toneladas da commodity, com produtividade 13% superior ao ano passado, e o MT, principal produtor de soja do País, estima fechar o ciclo com 28 milhões de toneladas, o melhor resultado das últimas três safras.

Nas lavouras de milho, a estimativa de produção também aumentou. Segundo a consultoria Safras & Mercado, a produção brasileira do grão deve chegar a 75,8 milhões de toneladas. Em janeiro de 2015, a previsão era de 74,6 milhões de toneladas.
