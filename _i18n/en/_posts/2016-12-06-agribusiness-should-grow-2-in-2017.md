---
layout: post
title:  "Agribusiness should grow 2% in 2017"
date:   2016-12-06 14:52:03 -0300
lang: en
main-img: "news-2016-12-06.jpg"
---

<p class="hint"><i class="fa fa-globe" aria-hidden="true"></i>This text was translated from Portuguese using <a href="https://translate.google.com/">Google Translator</a></p>

<p class="header">Projection is Confederation of Agriculture and Livestock of Brazil (CNA). Industry share in GDP expected to reach 3% in 2016</p>

Agribusiness is expected to expand by 2% in 2017, according to estimates by the Confederation of Agriculture and Livestock of Brazil (CNA), released on Tuesday (6). According to the confederation's estimate, the Gross Domestic Product (GDP) of agribusiness will grow between 2.5% and 3% in 2016.

The sector increased its share of GDP in 2015 for this year, with the percentage change from 21.5% to 23%. According to CNA's technical superintendent, Bruno Lucchi, the trend is for the growth of the percentage of participation of the sector in the economy.

For him, next year, the sugar-energy segment will expand, driven by rising sugar and ethanol prices. "Coffee still needs to recover production," he said. Lucchi added that the growth of other segments, such as animal protein, will depend on the recovery of the economy so that people have income to buy.

The agricultural sector accounts for 48% of the country's total exports, according to CNA. By 2016, agribusiness products should guarantee a significant commercial balance to the country: US $ 72.5 billion.

<hr>
Source: [Portal Brasil](http://www.brasil.gov.br/economia-e-emprego/2016/12/agronegocio-deve-ter-crescimento-de-2-em-2017)
