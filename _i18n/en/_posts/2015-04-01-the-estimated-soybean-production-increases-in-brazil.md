---
layout: post
title:  The estimated soybean production increases in Brazil
date:   2015-04-01 14:52:03 -0300
lang: en
main-img: news-2015-04-01.jpg
main-img-ref: ''
---

<p class="hint"><i class="fa fa-globe" aria-hidden="true"></i>This text was translated from Portuguese using <a href="https://translate.google.com/">Google Translator</a></p>

According to Agroconsult consultancy, the Brazilian soybean crop 2014/2015 should register new record with 95.8 million tons, 11% higher if compared to the previous harvest. The result mainly reflects the abundant and regular rainfall in the south of the country since the beginning of planting, which increased productivity in the region for about 50 bags per hectare and balanced part of the loss by drought in other locations such as Goiás, Minas Gerais and part of Bahia and Piauí.

The states of Rio Grande do Sul (RS) and Mato Grosso (MT) have been highlighted in this harvest. The RS should produce 15 million tons of soybean, with productivity 13% higher than last year, and the MT, Brazil's main soybean producer, estimates to close the cycle with 28 million tons, the best result of the last three harvests.

In the maize crops, the estimated production has also increased. According to Safras & Mercado consultancy, the Brazilian grain production should reach 75.8 million tons. In January 2015, the forecast was 74.6 million tons.
