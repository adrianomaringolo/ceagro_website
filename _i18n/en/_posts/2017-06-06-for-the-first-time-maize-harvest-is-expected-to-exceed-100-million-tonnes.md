---
layout: post
title:  "For the first time, maize harvest is expected to exceed 100 million tonnes"
date:   2017-06-06 00:00:00 -0300
lang: pt
main-img: "news-2017-06-06.jpg"
main-img-ref: ''
---
<p class="hint"><i class="fa fa-globe" aria-hidden="true"></i>This text was translated from Portuguese using <a href="https://translate.google.com/">Google Translator</a></p>

<p class="header">According to Céleres consulting, regular climate and "good progress" of the soybean crop contribute to the production of the grain</p>

The corn harvest in Brazil is expected to reach 100.7 million tons in the 2016/2017 harvest. The forecast is from consultancy Céleres, released on Tuesday (6). If confirmed, this will be the first time that production will exceed the level of 100 million tonnes. The record is 84.7 million tons, recorded in the 2014/15 harvest.

According to the consultancy, if the area of ​​the second corn crop is maintained at 11.3 million ha, an increase of 8.4% compared to last season, productivity in 2016/17 should be 5.9 t / ha, Which represents a recovery of 37% compared to 2015/16.

According to Célere, "the winter corn crop was benefited by the regular climate and within what is necessary in practically all the producing regions of the country" and "due to the good progress of the soybean harvest, which allowed the planting of the second crop to occur within Of the ideal. "

Céleres' projection for maize exports in 2017 is 30 million tons, an increase of 37% over the previous year. On the domestic market, the analysis states that "a scenario of replenishment is being drawn" and "a much more comfortable production balance than last year, with final inventories increasing to 16.4 million tons, twice as much as in the previous harvest."

<hr>
Source: [Portal Brasil](http://www.brasil.gov.br/economia-e-emprego/2017/06/pela-primeira-vez-colheita-de-milho-deve-superar-100-milhoes-de-toneladas)