---
layout: post
title:  Soybeans will have 18.5% higher production, of 113.9 million tons, says IBGE
date:   2017-06-08 00:00:00 -0300
lang: en
main-img: "news-2017-06-08.jpg"
main-img-ref: ''
---
<p class="hint"><i class="fa fa-globe" aria-hidden="true"></i></span>This text was translated from Portuguese using <a href="https://translate.google.com/">Google Translator</a></p>

The Systematic Survey of Agricultural Production in May estimates a record harvest of 113.9 million tons of soybeans, up 18.5% from production in 2016, according to the Brazilian Institute of Geography and Statistics (IBGE) . The area planted to soybeans is expected to rise 2.1% compared to 2016, to 33.9 million hectares. As a result, IBGE estimates an average yield of 3,367 kg / ha.

"In the current harvest, the largest producer is Mato Grosso, with 30.7 million tons, or 27% of the national total. Paraná is the second producer, with 19.5 million tons, or 17.2% of the national total. Rio Grande do Sul appears as third producer, participating with 16.3% of the national total, "says the note released by IBGE.

According to the institute, the estimate for soybean production rose again in May, because of Rio Grande do Sul, "which is expected to produce 18.6 million tons, an increase of 7.6% over the previous month" .

There was a positive revaluation of 0.6% in the area planted and to be harvested. "The average income increased by 1.3 million tons over the previous month, which increased 7.0%, mainly due to the climate that benefited the crops in the field," says IBGE.

<hr>
Source: [IstoÉ](http://istoe.com.br/soja-tera-producao-185-maior-de-1139-milhoes-de-toneladas-diz-ibge/)