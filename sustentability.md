---
layout: page
title: sustentability.title
namespace: sustentability
permalink: /sustentabilidade/
permalink_en: /sustentability/
header-image: true
---
<div class="tabs-container">
    <ul class="tab-area">
        <li class="tab n1 shadow-box" data-opens="t1" id=" {% t sustentability.responsability.id %}">
            <h2>{% t sustentability.responsability.title %}</h2>
        </li>

        <li class="tab n2  shadow-box" data-opens="t2" id=" {% t sustentability.safety.id %}">
            <h2> {% t sustentability.safety.title %}</h2>
        </li>
    </ul>

    <div class="tab-box" id="t1">

        <div class="background-inner-image" style="background-image: url('{{site.img_path}}/sustentability/environment.jpg')">
        <span class="caption"><a href='http://www.freepik.com/free-photo/freshly-watered-plant_1077710.htm'>Designed by Freepik</a></span>
        </div>
    
        {% t sustentability.responsability.text %}
        
    </div>

    <div class="tab-box" id="t2">
        <div class="background-inner-image" style="background-image: url('{{site.img_path}}/sustentability/security.jpg')">
            <span class="caption"><a href='http://www.freepik.com/free-photo/safety-protective-equipment_1180857.htm'>Designed by Freepik</a></span>
        </div>

        {% t sustentability.safety.text %}
    </div>
</div>





