---
layout: page
title: activities.title
namespace: activities
permalink: /atividades/
permalink_en: /activities/
header-image: true
main-phrase-tkey: activities.main-phrase
---

<div class="tabs-container">
    <ul class="tab-area">
        <li class="tab n1 shadow-box" data-opens="t1" id="{% t activities.agribusiness.id %}">
            <h2>{% t activities.agribusiness.title %}</h2>
        </li>

        <li class="tab n2  shadow-box" data-opens="t2" id="{% t activities.storage.id %}">
            <h2>{% t activities.storage.title %}</h2>
        </li>

        <li class="tab n3  shadow-box" data-opens="t3" id="{% t activities.logistics.id %}">
            <h2>{% t activities.logistics.title %}</h2>
        </li>
    </ul>

    <div class="tab-box" id="t1">{% t activities.agribusiness.text %}</div>

    <div class="tab-box" id="t2">
        <div class="background-inner-image" style="background-image: url('{{site.img_path}}/activities/storage.jpg')">
        </div>
        {% t activities.storage.text %}
    </div>

    <div class="tab-box" id="t3">
        <div class="background-inner-image" style="background-image: url('{{site.img_path}}/activities/truck.jpg')">
            <span class="phrase">{% t activities.logistics.phrase %}</span>
            <span class="caption"><a href="http://br.freepik.com/fotos-gratis/transporte-rodoviario-por-caminhao_1181540.htm">Designed by Freepik</a></span>
        </div>
        {% t activities.logistics.text %}
    </div>
</div>